package sebekji1.cvi2.client;

import java.io.FileNotFoundException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import sebekji1.cvi2.server.CsvReader;
import sebekji1.cvi2.server.DBExistException;
import sebekji1.cvi2.server.DBNotFoundException;
import sebekji1.cvi2.server.DuplicateKeyException;
import sebekji1.cvi2.server.KeyNotFoundException;
import sebekji1.cvi2.compute.*;

public class Client {
/*
	private static final String K_listDB = "listdb";
	private static final String K_createDB = "createdb";
	private static final String K_insert = "insert";
	private static final String K_update = "update";
	private static final String K_get = "get";
	private static final String K_getA = "geta";
	private static final String K_flush = "flush";
*/
	public static void main(String[] args) {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}

		String host;
		int PORT;
		String conf_file;

		if (args.length != 3) {
			// <IP_adresa> <cislo_portu> <konf_soubor>

			host = "localhost"; //host = "127.0.0.1";
			PORT = 7777; 
			conf_file = "./data/conf.csv"; // konfig. soubor
			// System.exit(0);
		} else {
			host = args[0]; 
			PORT = Integer.valueOf(args[1]); 
			conf_file = args[2]; 
		}

		try {
			// vyhledani vzdaleneho objektu
			ServerInterface serveri;
			String name = "muj_server";
			Registry registry = LocateRegistry.getRegistry(host, PORT);

			 serveri = (ServerInterface) registry.lookup(name);
			//serveri = (ServerInterface) Naming.lookup("//"+host+":"+PORT+"/"+name);
			 CsvReader reader = new CsvReader(conf_file, ';');
			 while (reader.readRecord()) {
                		try {
                    			switch (reader.get(0)) {
                        			case "listdb":
                            				System.out.print("Databases: ");
                            				for (String database : serveri.listDB()) {
                                				System.out.print("'" + database + "' ");
                            				}
							System.out.println();
							break;
                        			case "createdb":
						    System.out.println(String.format("Creating db \"%s\"", reader.get(1)));
						    serveri.createDB(reader.get(1));
						    System.out.println(String.format("Database \"%s\" created", reader.get(1)));
						    break;
						case "insert":
						    System.out.println(String.format("Inserting into database \"%s\" record [\"%s\";\"%s\"]", reader.get(1), reader.get(2), reader.get(3)));
						    serveri.insert(reader.get(1), Integer.valueOf(reader.get(2)), reader.get(3));
						    System.out.println(String.format("DB \"%s\" - record inserted", reader.get(1)));
						    break;
						case "update":
						    System.out.println(String.format("Updating record \"%s\" record in database \"%s\" with \"%s\"", reader.get(2), reader.get(1), reader.get(3)));
						    serveri.update(reader.get(1), Integer.valueOf(reader.get(2)), reader.get(3));
						    System.out.println(String.format("Db \"%s\" - record updated", reader.get(1)));
						    break;
						case "flush":
						    serveri.flush();
						    break;
						case "get":
						    System.out.println(String.format("Db \"%s\", ID \"%s\" : \"%s\"", reader.get(1), reader.get(2), serveri.get(reader.get(1), Integer.valueOf(reader.get(2))).getMessage()));
						    break;
						case "geta":
						    DBRecord[] result = serveri.getA(reader.get(1), new Integer[] {Integer.valueOf(reader.get(2)), Integer.valueOf(reader.get(3))});
						    System.out.println(String.format("Db \"%s\", IDs \"%s\", \"%s\" : \"%s\" and \"%s\"", reader.get(1), reader.get(2), reader.get(3), result[0].getMessage(), result[1].getMessage()));
						    break;
						default:
						    System.out.println(String.format("ERROR - command \"%s\" not implemented", reader.get(0)));
					    }
				} catch (RemoteException ex) {
				    System.out.println(ex.getMessage().substring(ex.getMessage().indexOf("ERORR")));
				}
			    }

			} catch (Exception e) {
				System.err.println("Data exception: " + e.getMessage());
		}
	}
}
