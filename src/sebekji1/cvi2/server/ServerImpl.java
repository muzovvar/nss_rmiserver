
package sebekji1.cvi2.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.util.HashMap;

import sebekji1.cvi2.compute.DBRecord;
import sebekji1.cvi2.compute.ServerInterface;

import sebekji1.cvi2.server.DBExistException;
import sebekji1.cvi2.server.DBNotFoundException;
import sebekji1.cvi2.server.DuplicateKeyException;
import sebekji1.cvi2.server.KeyNotFoundException;

public class ServerImpl implements ServerInterface {

	CsvWriter writer;
	CsvReader reader;
	static final String data_directory = System.getProperty("user.dir")+ System.getProperty("file.separator")+"data"+System.getProperty("file.separator");
	HashMap<String, DB> map;
	DBRecord[] zaznamy = null;
	File dbfile;
	

	public ServerImpl() {
		map = new HashMap<String, DB>();

		//File directory = new File(DATA_PATH);
		File dir = new File(data_directory);
		
    
	    /*
	    // list the files using our FileFilter
	    File[] files = dir.listFiles(new MyFileFilter());
	    for (File f : files)
	    {
	      System.out.println("file: " + f.getName());
	    }
	    */
		
		String[] list = dir.list(new MyFileFilter());//filter
		for (int i = 0; i < list.length; i++) {
			list[i] = list[i].substring(0, list[i].length() - 6); // 6charu=.dbcsv
		}

		for (int i = 0; i < list.length; i++) {
			String dbname = list[i];
			DB db = new DB(dbname);

			try {
				reader = new CsvReader(data_directory + dbname + ".dbcsv", ';', Charset.forName("UTF-8"));

				while (reader.readRecord()) {

					DBRecord radek = new DBRecord(reader.get(0), reader.get(1), Integer.valueOf(reader.get(2)), reader.get(3));

					db.addRecord(radek);
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			map.put(dbname, db);
		}
	}


	@Override
	public String[] listDB() {
    //todo
		return (String[]) map.keySet().toArray(new String[map.size()]);
	}


	@Override
	public boolean createDB(String dbname) throws DBExistException {
    //todo
		if (map.containsKey(dbname)) {
			throw new DBExistException("ERORR - Databze se jmenem " + dbname + " uz existuje");
		}

		map.put(dbname, new DB(dbname));
		return true;

	}

	@Override
	public Integer insert(String dbname, Integer key, String message)
			throws DBNotFoundException, DuplicateKeyException {
    //todo
		
		if (!map.containsKey(dbname)) {
			throw new DBNotFoundException("ERORR - Databze \"" + dbname + "\" neexistuje");
		}else if (map.get(dbname).foundKey(key)) {
			throw new DuplicateKeyException("ERORR - Duplikace kl - takov kl (\"" + key + "\") v databzi \"" + dbname + "\" u existuje.");
		}

		map.get(dbname).insert(key, message);
		
		return 0;
	}

	@Override
	public Integer update(String dbname, Integer key, String message)
			throws DBNotFoundException, KeyNotFoundException {
    //todo
		if (!map.containsKey(dbname)) {
			throw new DBNotFoundException("ERORR - Databze \"" + dbname + "\" nebyla nalezena.");
		}
		DB db = map.get(dbname);
		if (!db.foundKey(key)) {
			throw new KeyNotFoundException("ERORR - Takov kl (\"" + key + "\") v databzi nen.");
		}

		db.update(key, message);

		return 0;
	}

	@Override
	public DBRecord get(String dbname, Integer key) throws DBNotFoundException,
			KeyNotFoundException {
    //todo
		if (!map.containsKey(dbname)) {
			throw new DBNotFoundException("ERORR - Databze \"" + dbname + "\" nebyla nalezena.");
		}
		DB db = map.get(dbname);
		if (!db.foundKey(key)) {
			throw new KeyNotFoundException("ERORR - Takov kl (\"" + key + "\") v databzi nen.");
		}
		return db.get(key);

	}

	@Override
	public DBRecord[] getA(String dbname, Integer[] keys)
			throws DBNotFoundException, KeyNotFoundException {
		    //todo
		if (!map.containsKey(dbname)) {
			throw new DBNotFoundException("ERORR - Databze \"" + dbname + "\" nebyla nalezena.");
		}

		DB db = map.get(dbname);

		DBRecord[] result = new DBRecord[keys.length];
		
		for (int i = 0; i < keys.length; i++) {
			if (!db.foundKey(keys[i])) {
				throw new KeyNotFoundException("ERORR - Takov kl (\"" + keys[i] + "\") v databzi nen.");
			}
			for (int j = 0; j < db.getRecordList().length; j++) {

				if (db.getRecordList()[j].getKey() == keys[i]) {
					result[i] = db.getRecordList()[j];
				}
			}
		}
		return result;

	}

	/**
	 * Zapise zmeny na disk
	 * 
	 */
	@Override
	public void flush() {
    //todo
		String[] list = listDB();
		// projdu vsechny db
		for (int i = 0; i < list.length; i++) {
			String dbname = list[i];
			// z mapy si podle klice (jmena db) vytahnu instanci Databaze
			DB db = map.get(dbname);

			try {
				// instanci writeru s cestou pro soubor a kodovanim
				writer = new CsvWriter(data_directory + dbname + ".dbcsv", ';', Charset.forName("UTF-8"));
				// projdu vechny zaznamy v databazi
				for (int j = 0; j < db.getRecordList().length; j++) {
					DBRecord dbrecord = db.getRecordList()[j];
					writer.write(dbrecord.getTscreate());
					writer.write(dbrecord.getTsmodify());
					writer.write(String.valueOf(dbrecord.getKey()));
					writer.write(dbrecord.getMessage());
					writer.endRecord();	// konec
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			writer.close();	
		}
	
	}
}


